FROM busybox:1.35

RUN adduser -D static
USER static
WORKDIR /home/static

COPY --chmod=0644 index.html favicon.ico /home/static/

# Run BusyBox httpd
EXPOSE 3000
CMD ["busybox", "httpd", "-f", "-v", "-p", "3000", "-h", "/home/static"]

MAINTAINER "BSF <it@bibliosansfrontieres.org>"
